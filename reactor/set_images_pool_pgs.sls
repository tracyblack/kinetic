set_images_pool_pgs:
  local.cmd.run:
    - tgt: 'G@spawning:0 and G@type:cephmon'
    - tgt_type: compound
    - arg:
      - ceph osd pool set images pg_num {{ data['data']['pgs'] }}